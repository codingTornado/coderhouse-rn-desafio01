# Primer desafío

El proyecto es un tasklist muy sencillo. Permite agregar elementos, borrarlos y
marcarlos como resueltos. Hace uso de
https://github.com/oblador/react-native-vector-icons para los botones de agregar
y borrar. Para el checkbox usa
https://github.com/react-native-checkbox/react-native-checkbox
