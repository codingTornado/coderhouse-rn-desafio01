import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
} from "react-native";

import CheckBox from "@react-native-community/checkbox";

import DeleteButton from "./delete-button";

export default ListItem = ({ done, text, onValueChange, onDelete }) => {
  const [checked, setChecked] = useState(done);

  return (
    <View style={styles.container}>
      <CheckBox
        tintColors={{
          true: "#EBDBB2",
          false: "#EBDBB2",
        }}
        value={checked}
        onValueChange={() => {
          setChecked(!checked);
          onValueChange();
        }} />
      <Text style={[
        styles.text,
        !checked ? styles.pendingTask : styles.doneTask
      ]}>
        {text}
      </Text>
      <DeleteButton onDelete={onDelete} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 15,
    paddingVertical: 10,
  },

  doneTask: {
    color: "#A89984",
    textDecorationLine: "line-through",
  },

  pendingTask: {
    color: "#EBDBB2",
  },

  text: {
    marginLeft: 15,
    flexGrow: 1,
  }
});
