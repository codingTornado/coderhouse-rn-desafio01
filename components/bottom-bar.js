import React, { useState } from 'react';
import {
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default BottomBar = ({ onClick }) => {
  const [currentText, setCurrentText] = useState('');

  const handleAdd = () => {
    onClick(currentText);
    setCurrentText('');
  };

  return (
    <View style={styles.bottomBar}>
      <TextInput
        style={styles.textInput}
        onChangeText={(text) => setCurrentText(text)}
        value={currentText}
      />
      <TouchableOpacity onPress={handleAdd}>
        <Icon
          color={styles.addButton.color}
          name="plus-box"
          size={styles.addButton.fontSize} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  addButton: {
    color: "#EBDBB2",
    fontSize: 20,
  },

  textInput: {
    flexGrow: 1,
  },

  bottomBar: {
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
});
