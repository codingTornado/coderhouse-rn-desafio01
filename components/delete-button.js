import React, { useState } from "react";
import {
  StyleSheet,
  TouchableOpacity,
} from "react-native";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default DeleteButton = ({ onDelete }) => {
  return (
    <TouchableOpacity onPress={() => onDelete()}>
      <Icon
        color={styles.deleteIcon.color}
        name="trash-can-outline"
        size={styles.deleteIcon.fontSize} />
    </TouchableOpacity>
  )
};

const styles = StyleSheet.create({
  deleteIcon: {
    color: "#CC241D",
    fontSize: 20,
  },
});
