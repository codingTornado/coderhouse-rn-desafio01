import React, { useState } from 'react';
import {
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';

import ListItem from "./components/list-item";
import BottomBar from "./components/bottom-bar";

const App = () => {
  const [dataList, setDataList] = useState([]);
  const [lastId, setLastId] = useState(0);

  const handleDone = (id) => {
    const dataChanged = dataList.findIndex((data) => data.id == id);
    dataList[dataChanged].done = !dataList[dataChanged].done;
    setDataList(dataList);
  };

  const handleRemoveTask = (id) => {
    setDataList(dataList.filter((data) => data.id != id));
  };

  const handleAddTask = (text) => {
    setDataList([
      ...dataList,
      { id: lastId + 1, done: false, text: text }
    ]);
    setLastId(lastId + 1);
  };

  return (
    <SafeAreaView>
      <View style={styles.theme}>
        <StatusBar barStyle={"light-content"} />
        <View style={styles.taskList}>
          <FlatList
            data={dataList}
            renderItem={({ item }) => {
              return (
                <ListItem
                  text={item.text}
                  done={item.done}
                  onValueChange={() => handleDone(item.id)}
                  onDelete={() => handleRemoveTask(item.id)} />
              );
            }}
            keyExtractor={item => item.id.toString()}
          />
        </View>
        <BottomBar onClick={handleAddTask} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  taskList: {
    backgroundColor: "#282828",
    color: "#EBDBB2",
    flexGrow: 1,
  },

  theme: {
    height: "100%",
  },
});

export default App;
